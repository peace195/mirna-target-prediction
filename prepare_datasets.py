import pandas as pd
from sklearn.model_selection import train_test_split
import os

for path in ['dataset/', 'dataset/cv/', 'dataset/cv/train/', 'dataset/cv/val/', 'dataset/test/']:
    if not os.path.exists(path):
        os.makedirs(path)

positive_df = pd.read_csv('positive_mapped.csv')
negative_df = pd.read_csv('negative_mapped.csv')

negative_df.rename(columns={'Mock-miR sequence': 'mock_mirna_sequence'}, inplace=True)

positive_train, positive_test = train_test_split(positive_df, test_size=0.2)
negative_train, negative_test = train_test_split(negative_df, test_size=0.2)

positive_train[['mirna_sequence', 'mrna_sequence']].to_csv('dataset/cv/positive_all.csv', index=False)
positive_test[['mirna_sequence', 'mrna_sequence']].to_csv('dataset/test/positive.csv', index=False)
negative_train[['mock_mirna_sequence', 'mrna_sequence']].to_csv('dataset/cv/negative_all.csv', index=False)
negative_test[['mock_mirna_sequence', 'mrna_sequence']].to_csv('dataset/test/negative.csv', index=False)

for i in range(1, 6):
    train, val = train_test_split(positive_train, test_size=0.2)
    train[['mirna_sequence', 'mrna_sequence']].to_csv('dataset/cv/train/positive_train_f{}.csv'.format(i), index=False)
    val[['mirna_sequence', 'mrna_sequence']].to_csv('dataset/cv/val/positive_val_f{}.csv'.format(i), index=False)

    train, val = train_test_split(negative_train, test_size=0.2)
    train[['mock_mirna_sequence', 'mrna_sequence']].to_csv('dataset/cv/train/negative_train_f{}.csv'.format(i), index=False)
    val[['mock_mirna_sequence', 'mrna_sequence']].to_csv('dataset/cv/val/negative_val_f{}.csv'.format(i), index=False)
