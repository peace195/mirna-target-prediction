import numpy as np
from sklearn import metrics
import math

def perfeval(predictions, Y_test, verbose=0):
  """
  reference: https://github.com/eleventh83/deepMiRGene/blob/master/reproduce/cv.py
  """
  class_label = np.uint8(np.argmax(predictions, axis=1))
  R = np.asarray(Y_test)
  R_one_hot = np.array([ [ float(i == label) for i in range(2) ] for label in R ])
  CM = metrics.confusion_matrix(R, class_label, labels=None)
  CM = np.double(CM)
  acc = (CM[0][0]+CM[1][1])/(CM[0][0]+CM[0][1]+CM[1][0]+CM[1][1])
  se = (CM[0][0])/(CM[0][0]+CM[0][1])
  sp = (CM[1][1])/(CM[1][0]+CM[1][1])
  f1 = (2*CM[0][0])/(2*CM[0][0]+CM[0][1]+CM[1][0])
  ppv = (CM[0][0])/(CM[0][0]+CM[1][0])
  mcc = (CM[0][0]*CM[1][1]-CM[0][1]*CM[1][0])/np.sqrt((CM[0][0]+CM[0][1])*(CM[0][0]+CM[1][0])*(CM[0][1]+CM[1][1])*(CM[1][0]+CM[1][1]))
  gmean = np.sqrt(se*sp)
  try:
    auroc = metrics.roc_auc_score(R_one_hot[:, 0], predictions[:, 0])
    aupr = metrics.average_precision_score(R_one_hot[:, 0], predictions[:, 0], average="micro")
  except ValueError:
    auroc = 0
    aupr = 0

  if verbose == 1:
    print("ACC:","{:.3f}".format(acc), "SE:","{:.3f}".format(se),"SP:","{:.3f}".format(sp),"F-Score:","{:.3f}".format(f1), "PPV:","{:.3f}".format(ppv),"gmean:","{:.3f}".format(gmean),"AUROC:","{:.3f}".format(auroc), "AUPR:","{:.3f}".format(aupr))

  return [se,sp,f1,ppv,gmean,auroc,aupr,acc,CM]


def wrtrst(filehandle, rst, nfold=0, nepoch=0):
  """
  reference: https://github.com/eleventh83/deepMiRGene/blob/master/reproduce/cv.py
  """
  filehandle.write(str(nfold+1)+" "+str(nepoch+1)+" ")
  filehandle.write("SE: %s SP: %s F-score: %s PPV: %s g-mean: %s AUROC: %s AUPR: %s ACC: %s\n" %
  ("{:.3f}".format(rst[0]),
  "{:.3f}".format(rst[1]),
  "{:.3f}".format(rst[2]),
  "{:.3f}".format(rst[3]),
  "{:.3f}".format(rst[4]),
  "{:.3f}".format(rst[5]),
  "{:.3f}".format(rst[6]),
  "{:.3f}".format(rst[7])))
  filehandle.flush()
  return