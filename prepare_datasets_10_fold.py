import pandas as pd
from sklearn.model_selection import KFold
import os

for path in ['dataset/10fold/', 'dataset/10fold/train/', 'dataset/10fold/val/']:
  if not os.path.exists(path):
    os.makedirs(path)

positive_df = pd.read_csv('positive_mapped.csv')
negative_df = pd.read_csv('negative_mapped.csv')
negative_df.rename(columns={'Mock-miR sequence': 'mock_mirna_sequence'}, inplace=True)

kf = KFold(n_splits=10)

i = 0
for train_index, test_index in kf.split(positive_df):
  i = i + 1
  positive_df.iloc[train_index][['mirna_sequence', 'mrna_sequence']].to_csv('dataset/10fold/train/positive_train_f{}.csv'.format(i), index=False)
  positive_df.iloc[test_index][['mirna_sequence', 'mrna_sequence']].to_csv('dataset/10fold/val/positive_val_f{}.csv'.format(i), index=False)

i = 0
for train_index, test_index in kf.split(negative_df):
  i = i + 1
  negative_df.iloc[train_index][['mock_mirna_sequence', 'mrna_sequence']].to_csv('dataset/10fold/train/negative_train_f{}.csv'.format(i), index=False)
  negative_df.iloc[test_index][['mock_mirna_sequence', 'mrna_sequence']].to_csv('dataset/10fold/val/negative_val_f{}.csv'.format(i), index=False)