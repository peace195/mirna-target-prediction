import matplotlib
matplotlib.use('Agg')
import numpy as np
from utils import *
from create_contact_maps import *
from ConvNet import *
from sklearn import metrics
import torch 
import torchvision
import torchvision.transforms as transforms
import torch.utils.data as data
from os.path import exists
from os import makedirs, environ
import torch.nn.functional as F
import torch.nn as nn
import math
import torch.utils.model_zoo as model_zoo
import sys
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator


## create directories for results and modelsgh
if not exists("./results/"):
  makedirs("./results/")

if not exists("./results"):
  makedirs("./results")

if not exists("./weights/"):
  makedirs("./weights/")

if not exists("./weights"):
  makedirs("./weights/")

class DriveData(data.Dataset):
  def __init__(self, pos_filename, neg_filename, transform=None):
    self.transform = transform
    pos_df = pd.read_csv(pos_filename)
    neg_df = pd.read_csv(neg_filename)
    pos_df.columns = ['mirna_sequence', 'mrna_sequence']
    neg_df.columns = ['mirna_sequence', 'mrna_sequence']
    pos_df['type'] = 'pos'
    neg_df['type'] = 'neg'
    merged = pd.concat([pos_df, neg_df])
    
    self.__xs = []
    self.__ys = []

    for idx, row in merged.iterrows():
      mirna = row['mirna_sequence']
      mrna = row['mrna_sequence']
      cm = get_contact_map(mrna, mirna)
      if cm is not None:
        self.__xs.append(cm)
        self.__ys.append(1 if row['type'] == 'pos' else 0)

  def __getitem__(self, index):
    return (self.__xs[index], self.__ys[index])

  def __len__(self):
    return len(self.__xs)

def update_lr(optimizer, lr):    
  for param_group in optimizer.param_groups:
    param_group['lr'] = lr

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
N_EPOCH = 80
BATCH_SIZE = 128
NUM_CLASSES = 2
LEARNING_RATE = 0.001


WriteFile = open("./results/test_variable.rst","w")
rst = []
loss_list = []
accuracy_list = []
model = ConvNet_v6().to(device)
model = model.double()
criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adagrad(model.parameters(), lr=LEARNING_RATE)
train_dataset = DriveData("./dataset/cv/positive_all.csv", "./dataset/cv/negative_all.csv")
test_dataset = DriveData("./dataset/test/positive.csv", "./dataset/test/negative.csv")
train_loader = torch.utils.data.DataLoader(dataset=train_dataset, batch_size=1, num_workers=8, shuffle=True)
test_loader = torch.utils.data.DataLoader(dataset=test_dataset, batch_size=1, num_workers=8, shuffle=False)
curr_lr = LEARNING_RATE
for epoch in range(N_EPOCH):
  print(epoch)
  correct = 0
  total = 0
  loss_total = 0
  for i, (seqs, labels) in enumerate(train_loader):
    seqs = seqs.to(device)
    labels = labels.to(device)
    outputs = model(seqs)
    _, predicted = torch.max(outputs.data, 1)
    total += labels.size(0)
    correct += (predicted == labels).sum().item()
    loss = criterion(outputs, labels)
    loss.backward()
    if i % BATCH_SIZE == BATCH_SIZE - 1 or i == len(train_loader) - 1:
      optimizer.step()
      optimizer.zero_grad()

    loss_total += loss.item()

  loss_list.append(loss_total)
  accuracy_list.append(float(correct) / total)

  _, ax1 = plt.subplots()
  ax2 = ax1.twinx()
  ax1.plot(loss_list)
  ax2.plot(accuracy_list, 'r')
  ax1.set_xlabel("Epoch")
  ax1.set_ylabel("Training loss")
  ax2.set_ylabel("Training accuracy")
  ax1.set_title("Training accuracy and loss")
  ax1.xaxis.set_major_locator(MaxNLocator(integer=True))
  plt.savefig("./results/accuracy_loss_variable.png", dpi=150)
  plt.close()

  # Test the model
  model.eval()
  with torch.no_grad():
    predictions = []
    Y_test = []
    for seqs, labels in test_loader:
      seqs = seqs.to(device)
      labels = labels.to(device)
      outputs = model(seqs)
      predictions.extend(outputs.data)
      Y_test.extend(labels)
    rst = perfeval(F.softmax(torch.stack(predictions), dim=1).cpu().numpy(), Y_test, verbose=1)
    wrtrst(WriteFile, rst, 0, epoch)
  
WriteFile.close()
torch.save(model.state_dict(), "./weights/test_variable.pt")
#model.load_state_dict(torch.load("./weights/%s_test_variable.pt" % TEST_SPECIES))