import pandas as pd
import mirmap
import numpy as np

CHANNEL = 4

def seq2num(a_seq):
  ints_ = [0]*len(a_seq)
  for i, c in enumerate(a_seq.lower()):
    if c == 'c':
      ints_[i] = 1
    elif c == 'g':
      ints_[i] = 2
    elif (c == 'u') | (c == 't'):
      ints_[i] = 3
  return ints_

def one_hot(X_enc):
  X_enc_len = len(X_enc)
  index_offset = np.arange(X_enc_len) * CHANNEL
  X_enc_vec = np.zeros((X_enc_len, CHANNEL))
  X_enc_vec.flat[index_offset + np.asarray(X_enc).ravel()] = 1
  return X_enc_vec

def find_longest_idx(pairings):
  longest_pairing_index = 0
  nr_max_nonzero = 0
  for idx, pairing in enumerate(pairings):
    nr_nonzero = sum([x != 0 for x in pairing])
    if nr_nonzero > nr_max_nonzero:
      longest_pairing_index = idx
      nr_max_nonzero = nr_nonzero
  return longest_pairing_index

def get_contact_map(mrna, mirna):
  mirna_len = len(mirna)
  mrna_len = len(mrna)
  
  mrna_hot_seq = one_hot(seq2num(mrna))
  mirna_hot_seq = one_hot(seq2num(mirna))

  v_enc = np.zeros((CHANNEL, mirna_len, mrna_len))
  h_enc = np.zeros((CHANNEL, mirna_len, mrna_len))

  for i in range(CHANNEL):
    v_enc[i] = np.tile(np.array([mrna_hot_seq[:, i]]), (mirna_len, 1))
    h_enc[i] = np.tile(np.array([mirna_hot_seq[:, i]]).transpose(), (1, mrna_len))
    
  mirna = mirna.upper().replace('T', 'U')
  mrna = mrna.upper().replace('T', 'U')
  length_diff = 10
  mrna = '-' * (length_diff) + mrna + '-' * (length_diff)
  mim = mirmap.mm(mrna, mirna)
  mim.find_potential_targets_with_seed(mirna_start_pairing=1,
                        allowed_lengths=[10],
                        allowed_gu_wobbles={10:0},
                        allowed_mismatches={10:4})
  
  if len(mim.end_sites) == 0:
    return None
  
  cm = np.zeros((mirna_len, mrna_len))
  
  longest_idx = find_longest_idx(mim.pairings)
  mrna_start_idx = mim.end_sites[longest_idx] - 10 - 10 # 10 for target length, 10 for padding
  mirna_start_idx = mirna_len - 10 # 10 for target length
  for idx, val in enumerate(mim.pairings[longest_idx]):
    if val != 0:
      if (mrna_start_idx + idx < mrna_len):
        cm[mirna_start_idx + idx][mrna_start_idx + idx] = 10

  hv_end = np.concatenate((h_enc, v_enc, np.expand_dims(cm, axis=0)), axis=0)
  
  return hv_end

'''
pos_df = pd.read_csv('dataset/cv/positive_all.csv')
neg_df = pd.read_csv('dataset/cv/negative_all.csv')
pos_df.columns = ['mirna_sequence', 'mrna_sequence']
neg_df.columns = ['mirna_sequence', 'mrna_sequence']
pos_df['type'] = 'pos'
neg_df['type'] = 'neg'
merged = pd.concat([pos_df, neg_df])

xs = []
ys = []

for idx, row in merged.iterrows():
  mirna = row['mirna_sequence']
  mrna = row['mrna_sequence']
  cm = get_contact_map(mrna, mirna)
  if cm is not None:
    xs.append(cm)
    ys.append(1 if row['type'] == 'pos' else 0)
'''