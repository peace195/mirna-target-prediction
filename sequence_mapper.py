import pandas as pd
from Bio import SeqIO
from math import nan

positive_df = pd.read_csv('positive.csv')
negative_df = pd.read_csv('negative.csv')

mature_sequences = {}
hairpin_sequences = {}
utr_sequences = {}

for record in SeqIO.parse('miRBase/mature.fa', 'fasta'):
    mature_sequences[record.id] = str(record.seq)

for record in SeqIO.parse('miRBase/hairpin.fa', 'fasta'):
    hairpin_sequences[record.id] = str(record.seq)

for record in SeqIO.parse('HS_3prime_UTR_Sequences.fa', 'fasta'):
    record_id = record.id
    record_id = record_id[:record_id.index('|')]
    utr_sequences[record_id] = str(record.seq)

positive_mirna_sequences = []
positive_mrna_sequences = []
for index, sequence in positive_df.iterrows():
    mir_id = sequence['miR_ID']
    mrna_id = sequence['mRNA_ID']
    start = sequence['Start_position'] - 1
    end = sequence['End_position']

    if mir_id in mature_sequences:
        positive_mirna_sequences.append(mature_sequences[mir_id])
    elif mir_id in hairpin_sequences:
        positive_mirna_sequences.append(hairpin_sequences[mir_id])
    else:
        positive_mirna_sequences.append('')

    if mrna_id in utr_sequences:
        positive_mrna_sequences.append(utr_sequences[mrna_id][start:end])
    else:
        positive_mrna_sequences.append('')

positive_df['mirna_sequence'] = positive_mirna_sequences
positive_df['mrna_sequence'] = positive_mrna_sequences
positive_df = positive_df[positive_df['mirna_sequence'] != '']
positive_df = positive_df[positive_df['mrna_sequence'] != '']
positive_df.dropna(inplace=True)

negative_mrna_sequences = []
for index, sequence in negative_df.iterrows():
    sequence_id = sequence['mRNA_ID']
    start = sequence['Start_position'] - 1
    end = sequence['End_position']

    if sequence_id in utr_sequences:
        negative_mrna_sequences.append(utr_sequences[sequence_id][start:end])
    else:
        negative_mrna_sequences.append('')

negative_df['mrna_sequence'] = negative_mrna_sequences
negative_df = negative_df[negative_df['mrna_sequence'] != '']
negative_df.dropna(inplace=True)

positive_df.to_csv('positive_mapped.csv', index=False)
negative_df.to_csv('negative_mapped.csv', index=False)
