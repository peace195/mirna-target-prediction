import matplotlib
matplotlib.use('Agg')
import numpy as np
from utils import *
from create_contact_maps import *
from ConvNet import *
from sklearn import metrics
import torch 
import torchvision
import torchvision.transforms as transforms
import torch.utils.data as data
from os.path import exists
from os import makedirs, environ
import torch.nn.functional as F
import torch.nn as nn
import math
import torch.utils.model_zoo as model_zoo
import sys
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator


## create directories for results and modelsgh
if not exists("./results/"):
  makedirs("./results/")

if not exists("./results"):
  makedirs("./results")

if not exists("./weights/"):
  makedirs("./weights/")

if not exists("./weights"):
  makedirs("./weights/")

class DriveData(data.Dataset):
  def __init__(self, pos_filename, neg_filename, transform=None):
    self.transform = transform
    pos_df = pd.read_csv(pos_filename)
    neg_df = pd.read_csv(neg_filename)
    pos_df.columns = ['mirna_sequence', 'mrna_sequence']
    neg_df.columns = ['mirna_sequence', 'mrna_sequence']
    pos_df['type'] = 'pos'
    neg_df['type'] = 'neg'
    merged = pd.concat([pos_df, neg_df])
    
    self.__xs = []
    self.__ys = []

    for idx, row in merged.iterrows():
      mirna = row['mirna_sequence']
      mrna = row['mrna_sequence']
      cm = get_contact_map(mrna, mirna)
      if cm is not None:
        self.__xs.append(cm)
        self.__ys.append(1 if row['type'] == 'pos' else 0)

  def __getitem__(self, index):
    return (self.__xs[index], self.__ys[index])

  def __len__(self):
    return len(self.__xs)

def update_lr(optimizer, lr):    
  for param_group in optimizer.param_groups:
    param_group['lr'] = lr

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
N_EPOCH = 80
BATCH_SIZE = 128
NUM_CLASSES = 2
LEARNING_RATE = 0.001
K_FOLD = 10

WriteFile = open("./results/cv_variable.rst" ,"w")
rst = []
for fold in range(K_FOLD):
  loss_list = []
  accuracy_list = []
  model = ConvNet_v6().to(device)
  model = model.double()
  criterion = nn.CrossEntropyLoss()
  optimizer = torch.optim.Adagrad(model.parameters(), lr=LEARNING_RATE)
  train_dataset = DriveData("./dataset/10fold/train/positive_train_f%d.csv" % (fold+1), "./dataset/10fold/train/negative_train_f%d.csv" % (fold+1))
  test_dataset = DriveData("./dataset/10fold/val/positive_val_f%d.csv" % (fold+1), "./dataset/10fold/val/negative_val_f%d.csv" % (fold+1))
  train_loader = torch.utils.data.DataLoader(dataset=train_dataset, batch_size=1, num_workers=8, shuffle=True)
  test_loader = torch.utils.data.DataLoader(dataset=test_dataset, batch_size=1, num_workers=8, shuffle=False)
  total_step = len(train_loader)
  for epoch in range(N_EPOCH):
    correct = 0
    total = 0
    loss_total = 0
    print(epoch)
    for i, (seqs, labels) in enumerate(train_loader):
      seqs = seqs.to(device)
      labels = labels.to(device)
      outputs = model(seqs)
      _, predicted = torch.max(outputs.data, 1)
      loss = criterion(outputs, labels)
      loss.backward()
      if i % BATCH_SIZE == BATCH_SIZE - 1 or i == len(train_loader) - 1:
        optimizer.step()
        optimizer.zero_grad()

    # Test the model
    model.eval()
    with torch.no_grad():
      predictions = []
      Y_test = []
      for seqs, labels in test_loader:
        seqs = seqs.to(device)
        labels = labels.to(device)
        outputs = model(seqs)
        predictions.extend(outputs.data)
        _, predicted = torch.max(outputs.data, 1)
        Y_test.extend(labels)
        total += labels.size(0)
        correct += (predicted == labels).sum().item()
        loss_total += criterion(outputs, labels).item()

      wrtrst(WriteFile, perfeval(F.softmax(torch.stack(predictions), dim=1).cpu().numpy(), Y_test, verbose=1), fold, epoch)
    
    loss_list.append(loss_total / total)
    accuracy_list.append(float(correct) / total)

  torch.save(model.state_dict(), "./weights/f%d_variable.pt" % (fold+1))
  model.eval()
  with torch.no_grad():
    predictions = []
    Y_test = []
    for seqs, labels in test_loader:
      seqs = seqs.to(device)
      labels = labels.to(device)
      outputs = model(seqs)
      predictions.extend(outputs.data)
      Y_test.extend(labels)
    rst.append(perfeval(F.softmax(torch.stack(predictions), dim=1).cpu().numpy(), Y_test, verbose=0))

rst_avg = np.mean([f[:-1] for f in rst],axis=0)
print(rst_avg)
wrtrst(WriteFile, rst_avg, 0, 0)
WriteFile.close()