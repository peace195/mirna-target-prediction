import pandas as pd
import mirmap

seed_match_types = {
    #'Seed_match_8mer': {'mirna_start_pairing': 1, 'allowed_lengths': [8], 'allowed_gu_wobbles': {8:0}, 'allowed_mismatches': {8:0}},
    #'Seed_match_8merA1': {'mirna_start_pairing': 1, 'allowed_lengths': [8], 'allowed_gu_wobbles': {8:0}, 'allowed_mismatches': {8:1}},
    #'Seed_mach7mer1': {'mirna_start_pairing': 1, 'allowed_lengths': [7], 'allowed_gu_wobbles': {7:0}, 'allowed_mismatches': {7:0}},
    #'Seed_match7mer2': {'mirna_start_pairing': 2, 'allowed_lengths': [7], 'allowed_gu_wobbles': {7:0}, 'allowed_mismatches': {7:0}},
    #'Seed_match7merA1': {'mirna_start_pairing': 1, 'allowed_lengths': [7], 'allowed_gu_wobbles': {7:0}, 'allowed_mismatches': {7:1}},
    #'Seed_match6mer1': {'mirna_start_pairing': 1, 'allowed_lengths': [6], 'allowed_gu_wobbles': {6:0}, 'allowed_mismatches': {6:0}},
    #'Seed_match6mer2': {'mirna_start_pairing': 2, 'allowed_lengths': [6], 'allowed_gu_wobbles': {6:0}, 'allowed_mismatches': {6:1}},
    #'Seed_match6mer1GU': {'mirna_start_pairing': 1, 'allowed_lengths': [6], 'allowed_gu_wobbles': {6:1}, 'allowed_mismatches': {6:0}},
    #'Seed_match6mer2GU': {'mirna_start_pairing': 2, 'allowed_lengths': [6], 'allowed_gu_wobbles': {6:1}, 'allowed_mismatches': {6:0}},
    'Seed_match': {'mirna_start_pairing': 1, 'allowed_lengths': [10], 'allowed_gu_wobbles': {10:0}, 'allowed_mismatches': {10:4}},
}

pos_df = pd.read_csv('dataset/cv/positive_all.csv')
neg_df = pd.read_csv('dataset/cv/negative_all.csv')

found = 0
not_found = 0
for index, row in pos_df.iterrows():
    mirna_sequence = row['mirna_sequence'].upper().replace('T', 'U')
    mrna_sequence = row['mrna_sequence'].upper().replace('T', 'U')
    length_diff = 10
    mrna_sequence = '-' * (length_diff) + mrna_sequence + '-' * (length_diff)
    mim = mirmap.mm(mrna_sequence, mirna_sequence)
    for seed_match_type, parameters in seed_match_types.items():
        mim.find_potential_targets_with_seed(mirna_start_pairing=parameters['mirna_start_pairing'],
                                            allowed_lengths=parameters['allowed_lengths'],
                                            allowed_gu_wobbles=parameters['allowed_gu_wobbles'],
                                            allowed_mismatches=parameters['allowed_mismatches'])
        if len(mim.end_sites) > 0:
            found +=1
            break
        
    if len(mim.end_sites) == 0:
        print('miRNA: {},\t\t mRNA: {}'.format(mirna_sequence, mrna_sequence))
        not_found += 1

print('Positive Found: {}, not found: {}'.format(found, not_found))

found = 0
not_found = 0
for index, row in neg_df.iterrows():
    mirna_sequence = row['mock_mirna_sequence'].upper().replace('T', 'U')
    mrna_sequence = row['mrna_sequence'].upper().replace('T', 'U')
    length_diff = 10
    mrna_sequence = '-' * (length_diff) + mrna_sequence + '-' * (length_diff)
    mim = mirmap.mm(mrna_sequence, mirna_sequence)
    for seed_match_type, parameters in seed_match_types.items():
        mim.find_potential_targets_with_seed(mirna_start_pairing=parameters['mirna_start_pairing'],
                                            allowed_lengths=parameters['allowed_lengths'],
                                            allowed_gu_wobbles=parameters['allowed_gu_wobbles'],
                                            allowed_mismatches=parameters['allowed_mismatches'])
        if len(mim.end_sites) > 0:
            found +=1
            break
        
    if len(mim.end_sites) == 0:
        not_found += 1

print('Negative Found: {}, not found: {}'.format(found, not_found))