
### Road map

1. Site-level miRNA identification 

2. Split 80% training, 20% testing

3. Represent inputs

4. http://metarna.readthedocs.io/en/latest/#quickstart metarna to extract CTS (only for gene-level (UTR-level))

5. Site-level datasets https://genomebiology.biomedcentral.com/articles/10.1186/s13059-014-0500-5#Sec35

6. Deep Learning model

7. Compare to other methods using site-level inputs


### How to form the input?

1. Map the miRNA_ID to miRNA database

2. Extract the mRNA segment by start_position and end_position (https://www.biostars.org/p/1226/ or https://github.com/zavolanlab/MIRZAG/tree/master/data)

3. Dont use metarna library because it is an algorithm to predict target of miRNA based on a lot of features. Use find_potential_targets_with_seed() of https://mirmap.ezlab.org/docs/to align miRNA with mRNA -> contact map. Notice that len(seq_target) have to greater than len(seq_mirna). Therefore, you need pad seq_target with "-" or " " to the begin of seq_target in case len(seq_target) < len(seq_mirna)

4. Form the input with size 9 x 40 x 40


### Some notices:

1. Test our test set by other methods with site-level inputs such as: https://github.com/lanagarmire/MirMark, http://data.snu.ac.kr/pub/deepTarget/, http://hulab.ucf.edu/research/projects/miRNA/TarPmiR/. After that, we have their results to compare to our results.


